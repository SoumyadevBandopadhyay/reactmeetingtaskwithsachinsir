import React, { Component } from 'react'

class AddCounter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            checkArr:[],
        }
    }
    
    addCounter=()=>{
        this.setState({
            checkArr:[...this.state.checkArr, 1]
        },((err,res)=>{
                this.props.setStateValue(this.state.checkArr)
        }))      
    }

    render() {
        return (
            <div>
                <div className="box">
                   <p>Add Counter</p>  
                    <button onClick={this.addCounter}>
                        <i class="fas fa-plus-square"></i>
                    </button>  
                </div>
            </div>
        )
    }
}

export default AddCounter
