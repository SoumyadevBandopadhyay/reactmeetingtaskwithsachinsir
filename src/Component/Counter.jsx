import React, { Component } from 'react'

class Counter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count:0,
        }
        this.increment=this.increment.bind(this)
        this.decrement=this.decrement.bind(this)
    }
    
    increment(){
        this.setState({
            count: this.state.count+1,
        })
    }

    decrement(){
        this.setState({
            count:this.state.count-1,
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.increment}>
                 <i class="fas fa-plus-square"></i>
                </button>
                <button onClick={this.decrement}>
                 <i class="fas fa-minus-square"></i>
                </button>
                <p>{this.state.count}</p>
            </div>
        )
    }
}

export default Counter
