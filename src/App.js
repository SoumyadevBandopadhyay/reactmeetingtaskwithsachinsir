import './App.css';

import React, { Component } from 'react'
import Counter from './Component/Counter';
import AddCounter from './Component/AddCounter';

class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      checkVar:[],
    }
    this.setStateValue=this.setStateValue.bind(this)
  }
  
  setStateValue(val){
    this.setState({
      checkVar:val,
    })
  }
  
  render() {
    return (
      <div>
        <AddCounter setStateValue={this.setStateValue}/>
        {this.state.checkVar.length>0 ? 
        this.state.checkVar.map((eachVal)=>{
          return <Counter/>
        }):
        <div></div>}
        <Counter  />
        <Counter  />
        <Counter  />
      </div>
    )
  }
}

export default App

